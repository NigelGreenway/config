# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
export CARGO_PATH=$HOME/.cargo/bin
export PATH=$PATH:$CARGO_PATH
export PATH=~/.npm-global/bin:/home/nigel/.local/bin:~/linuxbrew/.linuxbrew/bin:$PATH
export EDITOR='nvim'
export UPDATE_ZSH_DAYS=13

ZSH_CUSTOM="$HOME/.config/zsh/custom"
ZSH_THEME="Futurepixels"

source $ZSH_CUSTOM/plugins/pm/zsh/pm.zsh

HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  composer
  docker
  fasd
  git
  npm
  systemd
  rust
  pm
  kubectl
)

source $ZSH/oh-my-zsh.sh

if command -v tmux > /dev/null; then
  [[ ! $TERM =~ screen ]] && [ -z $TMUX ] && exec tmux
fi

alias reload="source ~/.config/zsh/.zshrc; echo \"zsh config reloaded\""
alias lla="ls -al"
alias open="xdg-open"
alias pbcopy="xclip -selection clipboard"
alias get-last-commit-message="git lg -1 | cut -d \  -f2 | git log --format=%B -n 1"
alias vi=nvim

alias fh="history | grep $1"
alias current-branch="git branch -l | grep '*' | cut -d ' ' -f2"

# PM functions
# https://github.com/Angelmmiguel/pm
function pma() {
  pm add $($1:$(basename $(pwd)))
}
alias pmg="pm go"
alias pmrm="pm remove"
alias pml="pm list"
# end PM

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /usr/share/nvm/init-nvm.sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
