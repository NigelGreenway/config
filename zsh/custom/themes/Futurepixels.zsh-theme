#!/usr/bin/env zsh

ZSH_THEME_PROMPT_CHAR="%F{cyan}%% "


function is_a_git_directory {
    [[ -n "$(git rev-parse --is-inside-work-tree 2> /dev/null)" ]] && echo 1
}

function git_info {
    if [[ -n $(is_a_git_directory) ]]; then
        local git_status=$(git status -sb 2> /dev/null)

        local added_count=`echo "${git_status}" | grep "^A" | wc -l`
        local remove_count=`echo "${git_status}" | grep -E "^D" | wc -l`

        local tracked_count=`echo "${git_status}" | grep -E "^( M| D)" | wc -l`
        local staged_count=`echo "${git_status}" | grep -E "^(M)" | wc -l`
        local untracked_count=`echo "${git_status}" | grep -E "^\?\?" | wc -l`

        function is_ahead {
            local count=`echo "${git_status}" | sed -n 's/.*\(ahead [[:digit:]]*\).*/\1/p' | awk '{ print $2 }'`
            [[ ${count} -gt 0 ]] && echo "%F{green}${count}🡱"
        }

        function is_behind {
            local count=`echo "${git_status}" | sed -n 's/.*\(behind [[:digit:]]*\).*/\1/p' | awk '{ print $2}'`
            [[ ${count} -gt 0 ]] && echo "%F{red}${count}🡳"
        }

        function get_branch {
          if [[ $(git diff --shortstat 2> /dev/null | tail -n1) != "" || ${remove_count} -gt 0 || ${staged_count} -gt 0 || ${tracked_count} -gt 0 || ${untracked_count} -gt 0 ]]
          then
            #dirty
            echo "%F{red}${branch_name}"
          else
            #clean
            echo "%F{green}${branch_name}"
          fi
        }

        function get_stashed_count {
            local count=`git stash list| wc -l`
            [[ ${count} -gt 0 ]] && echo "%F{white}[%F{blue}${count}%F{white}]"
        }

        local project_path=`git rev-parse --show-toplevel`
        local branch_name="$(git rev-parse --abbrev-ref HEAD)"

        local ahead="$(is_ahead)"
        local behind="$(is_behind)"

        local branch="$(get_branch)"

        local stash_count="$(get_stashed_count)"

        echo "\n${branch} ${stash_count} ${ahead}${behind}"
    fi
}


PROMPT='%F{cyan}%~%F{cyan}$(git_info)
%F{white}${ZSH_THEME_PROMPT_CHAR}%F{white}'

