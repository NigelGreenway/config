#!/usr/bin/env bash

time=`date +%Y-%m-%d-%H-%M-%S`
dir="`xdg-user-dir PICTURES`/Screenshots"
file="${time}.png"

cd ${dir} && \
  maim -u -f png -s -b 2 -c 0.35,0.55,0.85,0.25 -l \
  | tee "${file}" \
  | xclip -selection clipboard -t image/png
