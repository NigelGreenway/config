# Config

Here are my collection of dotfiles via my config directory.

Add any directory you need to sync to this repository to `.followDirectories`.

Running `sh main -h` will give you all that you need ... I hope!

Any pull requests are always apreciated, but this is my own config and is currently bound to my set up. That should change in the coming months though.

## Post installation

### For tmux

Simply run the chosen terminal and press the keybinding `ctrl+A`, `I` and all plugins will be installed and tmux will restart.
